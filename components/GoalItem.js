import React from 'react';
import { StyleSheet, Text, View} from 'react-native';


const GoalItem = props => {
    return <View style={styles.listItem}>
        <Text>{props.title}</Text>
    </View>
}

const styles = StyleSheet.create({
    listItem: {
        borderBottomColor: 'black',
        margin: 10,
        borderWidth: 1,
        padding: 10, 
        backgroundColor: '#ccc'
        }
    })
export default GoalItem;