import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Button, ScrollView, FlatList } from 'react-native';

import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {
  const [enteredGoal, setEnteredGoal] = useState('');
  const [courseGoals, setCourseGoals] = useState([]);

  const addGoalHandler = goalTitle => {
    // We dont use this cause it the state might not be updated always
    // setCourseGoals([...courseGoals, enteredGoal])
    setCourseGoals(currentGoals => [...currentGoals, { key: Math.random().toString(), value: goalTitle}
    ]);
  };
  

  return (
      <View style={styles.screen}>
        <GoalInput onAddGoal={addGoalHandler}/>
        <FlatList keyExtractor={(item, index) => item.key} data={courseGoals} renderItem={itemData => <GoalItem title={itemData.item.value} />}/>
      </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  }
    
})